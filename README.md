# WayLesS
An operating system built on top of seL4

## Prerequisites
- repo
- make
- xargo
- rust nightly
- nasm

You also need some prerequisites to compile the seL4 kernel:
- python-ply
- python-tempita
- libxml2 or libxml2-utils (whatever provides xmllint for your distro)
- cpio

And you need another for pdclib:
- jam

## Running it
`make` will build everything

`make run` will build everything and spin up the emulator

`Ctrl-A x` quits qemu

##Docker
If you're a fan of docker there's a docker image on dockerhub 
at https://hub.docker.com/r/uelen/archlinux-wayless/
