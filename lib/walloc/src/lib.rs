#![feature(allocator)]
#![allocator]
#![no_std]
#![crate_name = "walloc"]

extern crate unwind;
extern crate libc;
use libc::memmove;

//Error out if address hasn't benn set
pub static mut FREE: usize = 0x00000000000; 


#[no_mangle]
pub extern fn __rust_allocate(size: usize, _align: usize) -> *mut libc::c_void {
    unsafe{
        let modulus = FREE%_align;
        if modulus == 0 {
            let f=FREE;
            FREE += size;
            return f as *mut libc::c_void;
        } else {
            let f=FREE+_align-modulus;
            FREE += size+_align-modulus;
            return f as *mut libc::c_void;
        }
    }
}

#[no_mangle]
pub extern fn __rust_deallocate(_ptr: *mut u8, _old_size: usize, _align: usize) {
}

#[no_mangle]
pub extern fn __rust_reallocate(ptr: *mut libc::c_void, _old_size: usize, size: usize,
                                                                _align: usize) -> *mut libc::c_void {
    let new_pointer = __rust_allocate(size,_align);
    unsafe{memmove(new_pointer,ptr,_old_size);}
    return new_pointer;
}

#[no_mangle]
pub extern fn __rust_reallocate_inplace(_ptr: *mut u8, old_size: usize,
                                                                                _size: usize, _align: usize) -> usize {
        old_size // this api is not supported by libc
}

#[no_mangle]
pub extern fn __rust_usable_size(size: usize, _align: usize) -> usize {
        size
}

