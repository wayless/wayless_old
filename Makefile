LINKSCRIPT := linker.ld
LINKFLAGS := -T $(LINKSCRIPT)
#LINKFLAGS += --gc-sections
LINKFLAGS += -Map map.txt
LINKFLAGS += -L ./
LINKFLAGS += -nostdlib
LINKFLAGS += -z max-page-size=0x1000

NASM = nasm -felf64
SOURCES = dependencies.asm stub.asm
OBJECTS:=$(patsubst %,build/%.o,$(basename $(SOURCES)))
SOURCES:=$(patsubst %,src/arch/x86_64/%,$(SOURCES))
TARGET = build/wayless
#RSOURCES := $(shell find src/ -name "*.rs")
KERNEL = seL4/images/kernel-x86_64-pc99
LIBS =  seL4/stage/x86/pc99/lib/libsel4muslcsys.a
LIBS += seL4/stage/x86/pc99/lib/libsel4.a
LIBS += seL4/stage/x86/pc99/lib/libsel4platsupport.a
LIBS += seL4/stage/x86/pc99/lib/libc.a
LIBS += seL4/stage/x86/pc99/lib/libutils.a
LIBS += lib/pdclib/pdclib.a
TARGET_TRIPLE = x86_64-wayless-sel4
RLIBS = target/$(TARGET_TRIPLE)/debug/libwayless.a

all: $(TARGET)

build: 
	mkdir -p build

lib/libc: submodules

submodules:
	git submodule update --init --recursive 

target/$(TARGET_TRIPLE)/debug/libwayless.a: FORCE lib/libc
	xargo build --target $(TARGET_TRIPLE)

target/$(TARGET_TRIPLE)/release/libwayless.a: FORCE lib/libc
	xargo build --release --target $(TARGET_TRIPLE)

clean:
	rm -rf build
	rm -f map.txt
	xargo clean

distclean: clean
	rm -rf seL4
	git submodule deinit --all

.ONESHELL:
$(KERNEL):
	mkdir -p seL4
	cd seL4
	repo init -u https://github.com/seL4/sel4test-manifest.git -m 5.0.x.xml
	repo sync
	ln -s ../.config .config
	make

lib/pdclib/.git: submodules

.ONESHELL:
lib/pdclib/pdclib.a: lib/pdclib/.git
	cd lib/pdclib
	PDCLIB_NO_TEST=T JAM_TOOLSET=gcc PDCLIB_PLATFORM=wayless jam

lib/syscall/.git: submodules

.ONESHELL:
lib/syscall/target/$(TARGET_TRIPLE)/release/libsyscall.a: lib/syscall/.git
	cd lib/syscall
	xargo build --release --target $(TARGET_TRIPLE)

test: $(KERNEL)
	make -C seL4/ simulate-x86_64

run: $(TARGET) $(KERNEL)
	qemu-system-x86_64 -vga cirrus -nographic -kernel $(KERNEL) -initrd $<

graphics: $(TARGET) $(KERNEL)
	qemu-system-x86_64 -vga cirrus -kernel $(KERNEL) -initrd $<

debug: $(TARGET) $(KERNEL)
	qemu-system-x86_64 -nographic -kernel $(KERNEL) -initrd $< -s

$(LIBS): $(KERNEL)

$(TARGET): $(OBJECTS) $(RLIBS) $(LIBS)
	$(LD) -o $@ $(LINKFLAGS) --start-group $^ --end-group

$(OBJECTS): | build

build/%.o: src/arch/x86_64/%.asm
	$(NASM) $< -o $@

FORCE: ;
