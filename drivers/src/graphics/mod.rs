/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use core;
use core::fmt::Write;
use core::result::Result;
use core::result::Result::Ok;
//use core::str::StrExt;
use core::convert::AsMut;
//pub static mut SCREEN: Screen = Screen{p: Pos{x: 0, y: 0}};
use seL4_bindings::{seL4,bootinfo};
use seL4_bindings::seL4_rust::*;

pub fn mint_cap_from_root(space: seL4::seL4_CNode,ptr: u64,depth: u8,badge_addr: u16) -> Result<(),(core::fmt::Error)>{
    unsafe {
    match seL4_CNode_Mint(
        space,//_service,
        ptr,//dest_index,
        depth,//dest_depth,
        bootinfo::seL4_CapInitThreadCNode as u64,
        bootinfo::seL4_CapIOSpace as u64,
        32,//src_depth,
        seL4::seL4_CapRights_new(0,1,1),//rights, grant,read,write
        seL4::seL4_CapData_Badge_new( badge_addr as u64)
        ) {
        0 => Ok(()),
        _ => Err(core::fmt::Error)
    }
    }
}
type VGA = [Char; 80*25];

struct Char {
        pub c: u8,
        pub color: Colour,
}
pub struct Screen {
    pub addr: u64
}
impl Screen {
    pub fn clear(&mut self) {
        let background = Colour::Black;
        for i in 0 .. 80 * 25 {
            unsafe {
                *((self.addr + i * 2) as *mut u16) = (background as u16) << 12;
            }
        }
    }
    pub fn new(addr: u64) -> Screen {
        Screen{ addr: addr }
    } 
    fn write_char(&mut self, s: u8, x: u16, y: u16) -> Result<(),(core::fmt::Error)> {
        unsafe {
            let screen = self.addr as *mut VGA;
            (*screen).as_mut()[(x + (y*80)) as usize] = self::Char{c: s, color: Colour::Green};
        }
        return Ok(());
    }
    pub fn spawn() {

    }
}

#[derive(Copy,Clone)]
enum Colour {
    Black      = 0,
    Blue       = 1,
    Green      = 2,
    Cyan       = 3,
    Red        = 4,
    Pink       = 5,
    Brown      = 6,
    LightGray  = 7,
    DarkGray   = 8,
    LightBlue  = 9,
    LightGreen = 10,
    LightCyan  = 11,
    LightRed   = 12,
    LightPink  = 13,
    Yellow     = 14,
    White      = 15,
}
