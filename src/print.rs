/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use core;
use core::fmt::Write;
use seL4_bindings::seL4;

pub struct DebugOut;
impl Write for DebugOut {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {              
        for &b in s.as_bytes() {                                           
            unsafe { seL4::seL4_DebugPutChar(b) };                     
        }                                                                  
        Ok(())                                                             
    }
}

macro_rules! print {
    ($($arg:tt)*) => (let _ = write!(DebugOut,$($arg)*););
}
macro_rules! println {
    () => (print!("\n"));
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"), $($arg)*));
}
