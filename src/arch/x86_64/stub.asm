;    Waylos, a kernel built in rust
;    Copyright (C) 2015 Waylon Cude
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
section .text
global start
start:
    ;jmp start ;for debugging
    ;zero registers
    ;xor rax,rax
    ;xor rbx,rbx
    ;xor rcx,rcx
    ;xor rdx,rdx
    ;xor r8,r8
    ;xor r9,r9
    ;xor r10,r10
    ;xor r11,r11
    ;xor r12,r12
    ;xor r13,r13
    ;xor r14,r14
    ;xor r15,r15
    mov rbp, rsp
    ;xor rsp,rsp
    mov rsp, stack_end
    mov rsi, heap_begin
    ;mov rsi, stack_end
    mov rdx, 0xDEADBEEF
    ;call looper
    extern main
    call main


.hang:
    jmp .hang

global looper
looper:
    jmp looper
    ret

section .bss
align 4096
RESB 4096  ; Buffer
stack_begin:
    RESB 4096*16  ; Reserve 4 KiB stack space
stack_end:
RESB 4096  ; Buffer
heap_begin:
    RESB 4096*16  ; Reserve 4 KiB heap space
heap_end:

; vim: ft=nasm
