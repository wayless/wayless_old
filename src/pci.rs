/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use core::fmt::Write;
use collections::Vec;
use seL4_bindings::{seL4,bootinfo};
use seL4_bindings::seL4_rust::*;

use print::DebugOut;

//extern {
//    fn seL4_X86_IOPort_Out32(cap: seL4::seL4_X86_IOPort,port: u16,data: u32) -> u32;
//    fn seL4_X86_IOPort_In32(cap: seL4::seL4_X86_IOPort,port: u16)-> [u32;2];
//}
pub fn read_pci(bus: u8,dev: u8,func: u8,offset: u8) -> Result<u32,u32> {
    let address = 0x80000000 | ((bus as u32) << 16) | ((dev as u32) << 11) | ((func as u32) << 8) | ((offset as u32) & 0xFC);
    //println!("scanning {:x}",address);
    unsafe {
        let err = seL4_X86_IOPort_Out32(bootinfo::seL4_CapIOPort as u64,0xCF8,address);
        for _ in 0..1000 {
            asm!("NOP");
        }
        if err != 0 {
            //TODO: propogate error up
            return Err(err);
        }
        match seL4_X86_IOPort_In32(bootinfo::seL4_CapIOPort as u64,0xCFC) {
            [0, a] => return Ok(a),
            [e@_, _] => return Err(e)
        };
    }

}
//pub fn find_device(id: u32) -> Option<u16> {
//    for bus in 0..255 {
//        for dev in 0..32 {
//            //for func in 0..8 {
//                let read_id = read_pci(bus,dev,0,0);
//                if read_id == id {
//                    return Some((((bus as u32) << 8) | ((dev as u32) << 3) | (0 as u32)) as u16)
//                } else if read_id != 0 {
//                    println!("PCI DEVICE FOUND AT {}",read_id);
//                }
//            //}
//        }
//    }
//    None
//}

#[derive(Debug,Copy,Clone)]
pub struct PCI {
    bus: u8,
    device: u8,
    func: u8,
    id: u32
}
impl PCI {
    pub fn new(bus: u8, dev: u8, func: u8, id: u32) -> PCI {
        PCI {
            bus: bus,
            device: dev,
            func: func,
            id: id
        }
    }
    pub fn get_id(&self) -> u32 {
        self.id
    }
    pub fn get_id_for_guard(&self) -> u16 {
        ((self.bus as u16) << 8) | ((self.device as u16) << 3) | (self.func as u16)
    }
    //This should only be in here if it's active
    pub fn read_pci(&self, offset: u8) -> u32 {
        read_pci(self.bus,self.device,self.func,offset).unwrap()
    }
}
pub fn scan_devices() -> Vec<PCI> {
    //Will use up ~7K of memory at the most
    let mut devices = Vec::new();
    for bus in 0..255 {
        for dev in 0..32 {
            //for func in 0..8 {
                let read_id = match read_pci(bus,dev,0,0) {
                    Ok(n) => n,
                    Err(e) => {println!("Error reading pci config: {}",e); continue}
                };
                if read_id != 0xFFFFFFFF {
                    //println!("PCI DEVICE FOUND AT {:x}",read_id);
                    //println!("DEV: {}",dev);
                    //println!("BUS: {}",bus);
                    devices.push(PCI::new(bus,dev,0,read_id));
                }
            //}
        }
    }
    devices
}


