/*  Wayless, an operating system built on seL4
    Copyright (C) 2017 Waylon Cude

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#![no_std]
#![feature(start,lang_items,asm,collections,box_syntax,slice_patterns)]
//extern crate sel4_sys;
//extern crate sel4;
use core::fmt::Write;
extern crate collections;
extern crate seL4_bindings;
extern crate unwind;
//use sel4_sys::{seL4_MessageInfo,seL4_CPtr};

#[macro_use]
pub mod print;
use print::DebugOut;

mod pci;

use seL4_bindings::{seL4,bootinfo};

extern crate walloc;
//use sel4_start::BOOTINFO;

extern {
    fn looper();
}

//mod thread;


#[no_mangle]
pub extern fn main(bootinfo: usize,heap_addr: usize,magic: usize)  {
    if magic != 0xDEADBEEF {
        println!("Invalid magic");
        loop {}
    } 
    unsafe {
        walloc::FREE = heap_addr;
    }
    println!("Hello {}!","world");
    let raw_boot_info = bootinfo as *mut bootinfo::seL4_BootInfo;
    let boot_info = unsafe {&mut *raw_boot_info};
    println!("numIOPTLevels: {:?}",boot_info.numIOPTLevels);
    //println!("GD5446 at {:?}", pci::find_device(0x00B81013));
    let pci_devices = pci::scan_devices();
    println!("All pci devices: {:?}",pci_devices);
    for pci in pci_devices.iter() {
        println!("Addr: {:x}",pci.get_id());
        println!("Subclass: {:x}",(pci.read_pci(8) & 0xFFFF0000) >> 4);
    }
    let cirrus = pci_devices.iter().filter(|x| x.get_id() == 0x00B81013).next();
    if cirrus.is_none() {
        println!("Cirrus graphics card not found");
    } else {
        println!("Addr for guard: {}",cirrus.unwrap().get_id_for_guard());
        println!("CIRRUSL {:?}",cirrus.unwrap());
    }
    //sel4_sys::sel4_BootInfo
    //let n = box 7;
    //to_string is also broken
    //for &byte in n.to_string().as_bytes() {
    //    unsafe { sel4_sys::seL4_DebugPutChar(byte) };
    //}
    //for &byte in stack_addr.to_string().as_bytes() {
    //    unsafe { sel4_sys::seL4_DebugPutChar(byte) };
    //}
    //unsafe { sel4_sys::seL4_DebugPutChar(b'#') };
}
/*fn print_thread() {
    println!("THREAD STARTED");
    loop {}
}*/
